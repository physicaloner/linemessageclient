void Line_Notify(String message);

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

// Config connect WiFi
#define WIFI_SSID "Miniboy"
#define WIFI_PASSWORD "allstar09"

// Line config
#define LINE_TOKEN "LINE_ACCESS_TOKEN"

String message1 = "พบค่าโวลต์ทางพอร์ต A0 : ";
String message2 = "อ่านค่า pH ได้ : ";
String messageVol = " V";
String messageTmp = " หน่วย";

ESP8266WiFiMulti WiFiMulti;

unsigned long int avgValue;
const int analogInPin = A0;
int buf[10], temp;
int sensorValue = 0;
float b;

// For temp
float pHVol_old;
float phValue_old;

void setup()
{
  Serial.begin(115200);
  delay(10);

  WiFi.mode(WIFI_STA);
  // connect to wifi.
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");

  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected wifi with ip: ");
  Serial.println(WiFi.localIP());
}

void loop()
{
  for (int i = 0; i < 10; i++)
  {
    buf[i] = analogRead(analogInPin);
    delay(10);
  }

  for (int i = 0; i < 9; i++)
  {
    for (int j = i + 1; j < 10; j++)
    {
      if (buf[i] > buf[j])
      {
        temp = buf[i];
        buf[i] = buf[j];
        buf[j] = temp;
      }
    }
  }

  avgValue = 0;

  for (int i = 2; i < 8; i++)
    avgValue += buf[i];

  float pHVol = (float)avgValue * 5.0 / 1024 / 6;
  float phValue = -5.70 * pHVol + 21.34;

  if(String(pHVol, 2) != String(pHVol, 2)){
    Serial.println(message1 + pHVol + messageVol);
    Line_Notify(message1 + pHVol + messageVol);
    pHVol_old = pHVol;
  }

  if(String(phValue, 2) != String(phValue_old, 2)){
    Serial.println(message2 + phValue + messageTmp);
    Line_Notify(message2 + phValue + messageTmp);
    phValue_old = phValue;
  }
  
  delay(1000);
}

void Line_Notify(String message)
{
  axTLS::WiFiClientSecure client; // If Error should delete axTLS

  if (!client.connect("notify-api.line.me", 443))
  {
    Serial.println("connection failed");
    return;
  }

  String req = "";
  req += "POST /api/notify HTTP/1.1\r\n";
  req += "Host: notify-api.line.me\r\n";
  req += "Authorization: Bearer " + String(LINE_TOKEN) + "\r\n";
  req += "Cache-Control: no-cache\r\n";
  req += "User-Agent: ESP8266\r\n";
  req += "Connection: close\r\n";
  req += "Content-Type: application/x-www-form-urlencoded\r\n";
  req += "Content-Length: " + String(String("message=" + message).length()) + "\r\n";
  req += "\r\n";
  req += "message=" + message;
  // Serial.println(req);
  client.print(req);

  delay(20);

  // Serial.println("-------------");
  while (client.connected())
  {
    String line = client.readStringUntil('\n');
    if (line == "\r")
    {
      break;
    }
    //Serial.println(line);
  }
  // Serial.println("-------------");
}
